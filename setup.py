from setuptools import find_packages, setup

setup(
    name='arrout',
    version='2020.5.20',
    package_dir={'': 'src'},
    packages=find_packages(where='src'),
    python_requires='>=3.8',
)
