class Options(object):
    debugging = False
    """Indicates whether debug messages should be output or not."""

    verbose = False
    """Indicates whether verbose messages should be output or not."""

    @staticmethod
    def debug_allowed() -> bool:
        """
        Check if debug messages are allowed or not.

        Returns
        -------
        bool
            True if allowed, else False.
        """
        return Options.debugging

    @staticmethod
    def verbose_allowed() -> bool:
        """
        Check if verbose messages are allowed or not.

        Returns
        -------
        bool
            True if allowed, else False.
        """
        return Options.verbose

    @staticmethod
    def config_debug_output(value: bool):
        Options.debugging = value

    @staticmethod
    def config_verbose_output(value: bool):
        Options.verbose = value
