from .options import Options

__all__ = [
    'log',
    'logd',
    'logv',
    'enable_debug_output',
    'enable_verbose_output',
    'disable_debug_output',
    'disable_verbose_output',
]


def log(*args, sep=' ', end='\n', file=None):
    print(*args, sep=sep, end=end, file=file)


def logd(*args, sep=' ', end='\n', file=None):
    if Options.verbose_allowed() or Options.debug_allowed():
        log(*args, sep=sep, end=end, file=file)


def logv(*args, sep=' ', end='\n', file=None):
    if Options.verbose_allowed():
        log(*args, sep=sep, end=end, file=file)


def enable_debug_output():
    Options.config_debug_output(True)


def disable_debug_output():
    Options.config_debug_output(False)


def enable_verbose_output():
    Options.config_verbose_output(True)


def disable_verbose_output():
    Options.config_verbose_output(False)
