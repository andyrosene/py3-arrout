# py3-arrout

This is my custom output package.

## Install - Local

Run the following command from within this directory:

``` bash
$ ./install.sh
```

## Uninstall - Local

Run the following command:

``` bash
$ ./uninstall.sh
```
