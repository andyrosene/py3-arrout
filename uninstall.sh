#!/bin/bash
# Uninstall this package.
#
# WARNING: Must be executed from within this directory.

#
###### Uninstall this package ######
#
python3.8 -m pip uninstall -y arrout
if [[ "$?" != "0" ]]; then
  echo "[ERROR] Something went wrong during the uninstallation"
  exit 1
fi
